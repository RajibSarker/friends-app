﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FriendsApp.Server.Migrations
{
    public partial class PublicIdcolumnintroduceforPhototable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PublicId",
                table: "Photo",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PublicId",
                table: "Photo");
        }
    }
}
