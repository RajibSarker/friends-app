﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FriendsApp.Server.Migrations
{
    public partial class addedtheLikeentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Photo_Users_UserId",
            //    table: "Photo");

            //migrationBuilder.DropPrimaryKey(
            //    name: "PK_Photo",
            //    table: "Photo");

            //migrationBuilder.RenameTable(
            //    name: "Photo",
            //    newName: "Photos");

            //migrationBuilder.RenameIndex(
            //    name: "IX_Photo_UserId",
            //    table: "Photos",
            //    newName: "IX_Photos_UserId");

            //migrationBuilder.AddPrimaryKey(
            //    name: "PK_Photos",
            //    table: "Photos",
            //    column: "Id");

            migrationBuilder.CreateTable(
                name: "Likes",
                columns: table => new
                {
                    LikerId = table.Column<int>(nullable: false),
                    LikeeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Likes", x => new { x.LikerId, x.LikeeId });
                    table.ForeignKey(
                        name: "FK_Likes_Users_LikeeId",
                        column: x => x.LikeeId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Likes_Users_LikerId",
                        column: x => x.LikerId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Likes_LikeeId",
                table: "Likes",
                column: "LikeeId");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Photos_Users_UserId",
            //    table: "Photos",
            //    column: "UserId",
            //    principalTable: "Users",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Photos_Users_UserId",
            //    table: "Photos");

            migrationBuilder.DropTable(
                name: "Likes");

            //migrationBuilder.DropPrimaryKey(
            //    name: "PK_Photos",
            //    table: "Photos");

            //migrationBuilder.RenameTable(
            //    name: "Photos",
            //    newName: "Photo");

            //migrationBuilder.RenameIndex(
            //    name: "IX_Photos_UserId",
            //    table: "Photo",
            //    newName: "IX_Photo_UserId");

            //migrationBuilder.AddPrimaryKey(
            //    name: "PK_Photo",
            //    table: "Photo",
            //    column: "Id");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Photo_Users_UserId",
            //    table: "Photo",
            //    column: "UserId",
            //    principalTable: "Users",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);
        }
    }
}
