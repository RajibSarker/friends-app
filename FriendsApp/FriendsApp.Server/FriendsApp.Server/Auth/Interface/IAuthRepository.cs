﻿using FriendsApp.Server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FriendsApp.Server.Auth.Interface
{
    public interface IAuthRepository
    {
        Task<User> Login(string userName, string password);
        Task<User> Register(User user, string password);
        Task<bool> UserExists(string userName);
    }
}
