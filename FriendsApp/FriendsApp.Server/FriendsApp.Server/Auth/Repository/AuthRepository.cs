﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FriendsApp.Server.Auth.Interface;
using FriendsApp.Server.Models;
using Microsoft.EntityFrameworkCore;

namespace FriendsApp.Server.Auth.Repository
{
    public class AuthRepository : IAuthRepository
    {
        private readonly DatabaseContext _context;

        public AuthRepository(DatabaseContext context)
        {
            _context = context;
        }
        public async Task<User> Login(string userName, string password)
        {
            if (String.IsNullOrEmpty(userName) || String.IsNullOrEmpty(password))
            {
                return null;
            }

            var existingUser =
                await _context.Users.Include(c=>c.Photos).FirstOrDefaultAsync(c => c.UserName.ToLower() == userName.ToLower());
            if (existingUser == null)
            {
                return null;
            }

            if (!VerifyingHashPassword(password, existingUser.PasswordHash, existingUser.PasswordSalt))
            {
                return null;
            }

            return existingUser;
        }
        private bool VerifyingHashPassword(string password, byte[] existingUserPasswordHash, byte[] existingUserPasswordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(existingUserPasswordSalt))
            {
                var computeHashPassword = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computeHashPassword.Length; i++)
                {
                    if (computeHashPassword[i] != existingUserPasswordHash[i])
                    {
                        return false;
                    }
                }
            }

            return true;
        }
        public async Task<User> Register(User user, string password)
        {

            if(user == null || String.IsNullOrEmpty(password))
            {
                return null;
            }

            byte[] passwordHash, passwordSalt;
            GeneratePasswordHash(password, out passwordHash, out passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
            await _context.AddAsync(user);
            await _context.SaveChangesAsync();
            return user;
        }
        private void GeneratePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }
        public async Task<bool> UserExists(string userName)
        {
            if (String.IsNullOrEmpty(userName))
            {
                return false;
            }

            var existingUse = await _context.Users.FirstOrDefaultAsync(c => c.UserName.ToLower() == userName.ToLower());
            if (existingUse == null)
            {
                return false;
            }

            return true;
        }
    }
}
