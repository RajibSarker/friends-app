﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FriendsApp.Server.Models;
using FriendsApp.Server.Models.DTO;
using Microsoft.EntityFrameworkCore.Migrations.Operations;

namespace FriendsApp.Server.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserListDto>()
                .ForMember(dest => dest.PhotoUrl,
                    opt => opt.MapFrom(
                        src => src.Photos.FirstOrDefault(c => c.IsMain).Url))
                .ForMember(dest => dest.Age,
                    opt => opt.MapFrom(
                        src => src.DateOfBirth.CalculateAge()));
            CreateMap<User, UserDetailDto>().ForMember(dest => dest.PhotoUrl,
                opt => opt.MapFrom(
                    src => src.Photos.FirstOrDefault(c => c.IsMain).Url))
                .ForMember(dest => dest.Age,
                    opt => opt.MapFrom(
                        src => src.DateOfBirth.CalculateAge()));
            CreateMap<Photo, PhotoDetailDto>();
            CreateMap<UserForUpdate, User>();
            CreateMap<PhotoForCreationDto, Photo>();
            CreateMap<Photo, PhotoForReturnDto>();
            CreateMap<UserRegistrationDTO, User>();
            CreateMap<MessageForCreation, Message>().ReverseMap();
            CreateMap<Message, MessageToReturnDto>()
                .ForMember(des => des.SenderPhotoUrl,
                    opt => opt.MapFrom(u => u.Sender.Photos.FirstOrDefault(c => c.IsMain).Url))
                .ForMember(des=>des.RecipientPhotoUrl,
                    opt=>opt.MapFrom(u=>u.Recipient.Photos.FirstOrDefault(c=>c.IsMain).Url));

            CreateMap<UserConnectionRequestDto, UserConnection>().ReverseMap();
        }
    }
}
