﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FriendsApp.Server.Helpers.Paging;
using Microsoft.AspNetCore.Http;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace FriendsApp.Server.Helpers
{
    public static class Extensions
    {
        public static void AddApplicationError(this HttpResponse response, string message)
        {
            response.Headers.Add("Application-Error",message);
            response.Headers.Add("Access-Control-Expose-Headers","Application-Error");
            response.Headers.Add("Access-Control-Allow-Origin","*");
        }
        //public static void AddPagination(this HttpResponse response, int currentPage, int itemsPerPage, int totalItems, int totalPages)
        //{
        //    // ... OR anonymous object ...
        //    var paginationHeader = new PaginationHeader(currentPage, itemsPerPage, totalItems, totalPages);

        //    //... format names of the HTTP parameters in HERADER to CAMELCASE
        //    var camelCaseFormater = new JsonSerializerSettings();
        //    camelCaseFormater.ContractResolver = new CamelCasePropertyNamesContractResolver();

        //    response.Headers.Add("Pagination", JsonConvert.SerializeObject(paginationHeader, camelCaseFormater));
        //    response.Headers.Add("Access-Control-Expose-Headers", "Pagination");
        //}

        public static void AddPagination(this HttpResponse response, int currentPage, int itemPerPage, int totalItems,
            int totalPages)
            {
            var paginationHeader = new PaginationHeader(currentPage, itemPerPage, totalItems, totalPages);
            var camelCaseFormatter = new JsonSerializerSettings();
            camelCaseFormatter.ContractResolver = new CamelCasePropertyNamesContractResolver();
            response.Headers.Add("Pagination", JsonConvert.SerializeObject(paginationHeader, camelCaseFormatter));
            response.Headers.Add("Access-Control-Expose-Headers", "Pagination");
        }
        public static int CalculateAge(this DateTime dateTime)
        {
            var age = DateTime.Now.Year - dateTime.Year;
            if (dateTime.AddYears(age) > DateTime.Now)
                return age--;
            return age;
        }
    }
}
