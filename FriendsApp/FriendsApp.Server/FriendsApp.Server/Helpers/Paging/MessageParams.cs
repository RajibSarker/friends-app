﻿namespace FriendsApp.Server.Helpers.Paging
{
    public class MessageParams
    {
        public int MaxPageSize { get; set; } = 50;
        public int PageNumber { get; set; }

        private int _pageSize = 10;
        public int PageSize
        {
            get { return _pageSize; }
            set => _pageSize = (value > MaxPageSize) ? MaxPageSize : value;
        }

        public int UserId { get; set; }
        public string MessageContainer { get; set; } = "UnRead";
    }
}
