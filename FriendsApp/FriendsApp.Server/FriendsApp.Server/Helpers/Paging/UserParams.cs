﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FriendsApp.Server.Helpers.Paging
{
    public class UserParams
    {
        public int MaxPageSize { get; set; } = 50;
        public int PageNumber { get; set; }

        private int _pageSize = 10;
        public int PageSize
        {
            get { return _pageSize; }
            set => _pageSize = (value > MaxPageSize) ? MaxPageSize : value;
        }

        public int UserId { get; set; }
        public string Gender { get; set; }
        public int MinAge { get; set; } = 18;
        public int MaxAge { get; set; } = 99;
        public string OrderBy { get; set; }
        public bool Likers { get; set; }
        public bool Likees { get; set; }
    }
}
