﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FriendsApp.Models.Hubs
{
    public class HubRequestDto
    {
        public int ClientId { get;set; }
        public string ConnectionId { get; set; }
    }
}
