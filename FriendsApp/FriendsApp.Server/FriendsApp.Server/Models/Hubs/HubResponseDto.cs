﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FriendsApp.Models.Hubs
{
    public class HubResponseDto
    {
        public int SenderId { get; set; }
        public int RecipientId { get; set; }
        public string ConnectionId { get; set; }
        public long UserId { get; set; }
        public long ErrorHeaderId { get; set; }
        public string Message { get; set; }
        public long TotalRowCount { get; set; }
        public long RowProcessCount { get; set; }
        public bool IsCompletedRowProcess { get; set; }
        public int FileUploadProcessType { get; set; }
        
        // file upload
        public long ProcessId { get; set; }
        public int FileUploadedStatus { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public long FileUploadType { get; set; }
        public long LogId { get; set; }
        public bool OnSuccess { get; set; }
        public bool OnFailed { get; set; }
        public bool IsProcessing { get; set; }
    }
}
