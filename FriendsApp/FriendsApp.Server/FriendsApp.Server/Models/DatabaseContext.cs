﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FriendsApp.Server.Models
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {

        }
        public DbSet<Value> Values { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<Like> Likes { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<UserConnection> UserConnections { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Like>().HasKey(c => new { c.LikerId, c.LikeeId });
            modelBuilder.Entity<Like>().HasOne(c => c.Likee).WithMany(c => c.Likers).HasForeignKey(c => c.LikeeId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Like>().HasOne(c => c.Liker).WithMany(c => c.Likees).HasForeignKey(c => c.LikerId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Message>().HasOne(c => c.Sender).WithMany(c => c.MessageSent).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Message>().HasOne(c => c.Recipient).WithMany(c => c.MessageReceived).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
