﻿using System;

namespace FriendsApp.Server.Models.DTO
{
    public class UserConnectionRequestDto
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public string ConnectionId { get; set; }
        public bool IsActive { get; set; }
        public string DeviceInfo { get; set; }

        public long CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? LastModifiedById { get; set; }
        public DateTime? LastModifiedOn { get; set; }
    }
}
