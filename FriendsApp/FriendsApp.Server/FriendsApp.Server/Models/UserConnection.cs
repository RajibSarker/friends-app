﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FriendsApp.Server.Models
{
    [Table("UserConnections")]
    public class UserConnection
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public string ConnectionId { get; set; }
        public bool IsActive { get; set; }
        public string DeviceInfo { get; set; }
    }
}
