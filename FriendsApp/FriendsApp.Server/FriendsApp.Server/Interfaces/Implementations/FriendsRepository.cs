﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FriendsApp.Models.Hubs;
using FriendsApp.Server.Helpers.Paging;
using FriendsApp.Server.Models;
using FriendsApp.Server.Models.Hubs;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;

namespace FriendsApp.Server.Interfaces.Implementations
{
    public class FriendsRepository : IFriendsRepository
    {
        private readonly DatabaseContext _context;
        private readonly IHubContext<MessageHub> _hubContext;

        public FriendsRepository(DatabaseContext context, IHubContext<MessageHub> hubContext)
        {
            _context = context;
            _hubContext = hubContext;
        }
        public async Task<bool> Add<T>(T entity) where T : class
        {
            _context.Add(entity);
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<bool> Remove<T>(T entity) where T : class
        {
            _context.Remove(entity);
            return await _context.SaveChangesAsync() > 0;
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<PagedList<User>> GetAllUsers(UserParams userParams)
        {
            var users = _context.Users.Include(c => c.Photos).OrderByDescending(c => c.LastActive).AsQueryable();
            users = users.Where(c => c.Id != userParams.UserId);
            users = users.Where(c => c.Gender == userParams.Gender);

            if (userParams.Likers)
            {
                var likers = await GetUserLikers(userParams.UserId, userParams.Likers);
                users = users.Where(c => likers.Contains(c.Id));
            }
            if (userParams.Likees)
            {
                var likees = await GetUserLikers(userParams.UserId, userParams.Likers);
                users = users.Where(c => likees.Contains(c.Id));
            }

            if (userParams.MinAge != 18 || userParams.MaxAge != 99)
            {
                var minDob = DateTime.Today.AddYears(-userParams.MaxAge - 1);
                var maxDob = DateTime.Today.AddYears(-userParams.MinAge);

                users = users.Where(c => c.DateOfBirth >= minDob && c.DateOfBirth <= maxDob);
            }

            if (!string.IsNullOrEmpty(userParams.OrderBy))
            {
                switch (userParams.OrderBy)
                {
                    case "created":
                        users = users.OrderByDescending(c => c.Created);
                        break;
                    default:
                        users = users.OrderByDescending(c => c.LastActive);
                        break;
                }
            }
            return await PagedList<User>.CreateAsync(users, userParams.PageNumber, userParams.PageSize);
        }

        private async Task<IEnumerable<int>> GetUserLikers(int userId, bool likers)
        {
            var user = await _context.Users.Include(c => c.Likers).Include(c => c.Likees)
                .FirstOrDefaultAsync(c => c.Id == userId);

            if (likers)
                return user.Likers.Where(c => c.LikeeId == userId).Select(c => c.LikerId);
            else
                return user.Likees.Where(c => c.LikerId == userId).Select(c => c.LikeeId);
        }

        public async Task<User> GetUser(int id)
        {
            if (id > 0)
            {
                return await _context.Users.Include(c => c.Photos).FirstOrDefaultAsync(c => c.Id == id);
            }
            else
            {
                return null;
            }
        }

        public async Task<Photo> GetPhoto(int id)
        {
            return await _context.Photos.FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<Photo> GetMainPhotForUser(int userId)
        {
            return await _context.Photos.Where(c => c.UserId == userId).FirstOrDefaultAsync(c => c.IsMain);
        }

        public async Task<Like> GetLike(int userId, int recipentId)
        {
            return await _context.Likes.FirstOrDefaultAsync(c => c.LikerId == userId && c.LikeeId == recipentId);
        }

        public async Task<Message> GetMessage(int id)
        {
            return await _context.Messages.FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<IEnumerable<Message>> GetMessageThread(int userId, int recipentId)
        {
            if (userId <= 0 || recipentId <= 0) throw new Exception("No userId or recipientId provided for message thread.");

            return await _context.Messages.Include(c => c.Sender).ThenInclude(c => c.Photos)
                .Include(c => c.Recipient).ThenInclude(c => c.Photos)
                .Where(c => c.RecipientId == userId && c.RecipientDeleted == false && c.SenderId == recipentId 
                || c.RecipientId == recipentId && c.SenderDeleted == false && c.SenderId == userId)
                .OrderByDescending(c => c.MessageSent).AsNoTracking().ToListAsync();
        }

        public async Task<PagedList<Message>> GetMessagesForUser(MessageParams messageParams)
        {
            if (messageParams == null) throw new Exception("Invalid input request.");

            var messages = _context.Messages.Include(c => c.Sender).ThenInclude(c => c.Photos)
                .Include(c => c.Recipient).ThenInclude(c => c.Photos).AsQueryable();

            switch (messageParams.MessageContainer)
            {
                case "Inbox":
                    messages = messages.Where(c => c.RecipientId == messageParams.UserId && c.RecipientDeleted == false);
                    break;
                case "Outbox":
                    messages = messages.Where(c => c.SenderId == messageParams.UserId && c.SenderDeleted == false);
                    break;
                default:
                    messages = messages.Where(c => c.RecipientId == messageParams.UserId && c.RecipientDeleted == false && c.IsRead == false);
                    break;
            }
            messages = messages.OrderByDescending(c => c.MessageSent);
            return await PagedList<Message>.CreateAsync(messages, messageParams.PageNumber, messageParams.PageSize);
        }


        public async Task<ICollection<UserConnection>> GetUserActiveConnections(long userId)
        {
            if (userId <= 0) return null;

            return await _context.UserConnections.Where(c => c.UserId == userId && c.IsActive == true).ToListAsync();
        }

        public async Task<bool> SendNotificationToUser(long userId, string eventName, HubResponseDto hubResponseDto)
        {
            if (userId <= 0 || string.IsNullOrEmpty(eventName) || hubResponseDto == null) return false;

            var userActiveConnections = await _context.UserConnections.Where(c => c.UserId == userId && c.IsActive == true).AsNoTracking().ToListAsync();
            if (userActiveConnections == null || !userActiveConnections.Any()) return false;


            //send response to user
            await SignalRHubEvents.SetHubEventToUsers(_hubContext, userActiveConnections.Select(c => c.ConnectionId).ToList(), eventName, hubResponseDto);
            return true;
        }

        public async Task<bool> UpdateUserConnection(string connectionId, bool isActive)
        {
            if (string.IsNullOrEmpty(connectionId)) return false;

            var existingUserConnection = await _context.UserConnections.FirstOrDefaultAsync(c => c.ConnectionId == connectionId && c.IsActive == true);
            if (existingUserConnection == null) return false;

            existingUserConnection.IsActive = false;

            _context.UserConnections.Update(existingUserConnection);
            return await _context.SaveChangesAsync() > 0;
        }
    }
}
