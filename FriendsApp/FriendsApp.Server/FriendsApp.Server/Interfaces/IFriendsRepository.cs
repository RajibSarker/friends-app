﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FriendsApp.Models.Hubs;
using FriendsApp.Server.Helpers.Paging;
using FriendsApp.Server.Models;

namespace FriendsApp.Server.Interfaces
{
    public interface IFriendsRepository
    {
        Task<bool> Add<T>(T entity) where T : class;
        Task<bool> Remove<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        Task<bool> SaveAll();
        Task<PagedList<User>> GetAllUsers(UserParams userParams);
        Task<User> GetUser(int id);
        Task<Photo> GetPhoto(int id);
        Task<Photo> GetMainPhotForUser(int userId);
        Task<Like> GetLike(int userId, int recipentId);
        Task<Message> GetMessage(int id);
        Task<PagedList<Message>> GetMessagesForUser(MessageParams messageParams);
        Task<IEnumerable<Message>> GetMessageThread(int userId, int recipentId);

        // manage user connections
        Task<ICollection<UserConnection>> GetUserActiveConnections(long userId);
        Task<bool> UpdateUserConnection(string connectionId, bool isActive);
        Task<bool> SendNotificationToUser(long userId, string eventName, HubResponseDto hubResponseDto);
    }
}
