﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using FriendsApp.Server.Helpers;
using FriendsApp.Server.Helpers.Paging;
using FriendsApp.Server.Interfaces;
using FriendsApp.Server.Models;
using FriendsApp.Server.Models.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FriendsApp.Server.Controllers
{
    [ServiceFilter(typeof(LogUserActivity))]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IFriendsRepository _repository;
        private readonly IMapper _mapper;

        public UsersController(IFriendsRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        // GET: api/Users
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] UserParams userParams)
        {
            var currentUserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
            userParams.UserId = currentUserId;

            var userFromRepo = await _repository.GetUser(currentUserId);

            if (string.IsNullOrEmpty(userParams.Gender))
            {
                userParams.Gender = userFromRepo.Gender == "male" ? "female" : "male";
            }

            var users = await _repository.GetAllUsers(userParams);
            var usersToReturn = _mapper.Map<ICollection<UserListDto>>(users);
            Response.AddPagination(users.CurrentPage, users.PageSize, users.TotalCount, users.TotalPages);
            return Ok(usersToReturn);
        }

        // GET: api/Users/5
        [HttpGet("{id}", Name = "GetUser")]
        public async Task<IActionResult> Get(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Invalid input request.");
            }

            var user = await _repository.GetUser(id);
            var userToReturn = _mapper.Map<UserDetailDto>(user);
            return Ok(userToReturn);
        }

        // POST: api/Users
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, UserForUpdate userForUpdate)
        {
            if (id != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();
            var userFromRepo = await _repository.GetUser(id);
            _mapper.Map(userForUpdate, userFromRepo);
            if (await _repository.SaveAll())
                return NoContent();
            throw new Exception($"Updating user {id} failed to save");
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        [HttpPost("{userId}/like/{recipentId}")]
        public async Task<IActionResult> LikeUser(int userId, int recipentId)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var like = await _repository.GetLike(userId, recipentId);
            if (like != null)
                return BadRequest("You already liked the user.");

            var user = await _repository.GetUser(recipentId);
            if (user == null)
                return NotFound();

            like = new Like
            {
                LikerId = userId,
                LikeeId = recipentId
            };

            if (await _repository.Add<Like>(like))
            {
                return Ok();
            }
            return BadRequest("Faild to like user.");
        }
    }
}
