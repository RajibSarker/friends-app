﻿using AutoMapper;
using Catalyzr.Engine.Models.Hubs;
using FriendsApp.Models.Hubs;
using FriendsApp.Server.Interfaces;
using FriendsApp.Server.Models;
using FriendsApp.Server.Models.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace FriendsApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ManageUserConnectionsController : Controller
    {
        private readonly IFriendsRepository _repository;
        private readonly IMapper _mapper;

        public ManageUserConnectionsController(IFriendsRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet("user/{userId}/activeConnections")]
        public async Task<IActionResult> GetUserActiveConnections(long userId)
        {
            if (userId <= 0) return BadRequest("Invalid input request.");

            return Ok(await _repository.GetUserActiveConnections(userId));
        }

        [HttpPost("addUserConnection")]
        public async Task<IActionResult> AddUserConnection([FromBody] UserConnectionRequestDto inputRequest)
        {
            if (inputRequest == null) return BadRequest("Invalid input request.");

            var data = _mapper.Map<UserConnection>(inputRequest);

            var isAdded = await _repository.Add(data);
            return Ok(isAdded);
        }

        [HttpPut("updateUserConnection")]
        public async Task<IActionResult> UpdateUserConnection([FromBody] UserConnectionRequestDto inputRequest)
        {
            if (inputRequest == null) return BadRequest("Invalid input request.");

            var isUpdated = await _repository.UpdateUserConnection(inputRequest.ConnectionId, inputRequest.IsActive);
            return Ok(isUpdated);
        }

        [HttpGet("{userId}/user/{eventName}/event/{message}/message")]
        public async Task<IActionResult> SendNotificationToUser(long userId, string eventName, string message)
        {
            if (userId <= 0 || string.IsNullOrEmpty(message)) return BadRequest("Invalid input request.");
            var responseDto = new HubResponseDto()
            {
                Message = message
            };

            return Ok(await _repository.SendNotificationToUser(userId, eventName, responseDto));
        }
    }
}
