﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FriendsApp.Server.Auth.Interface;
using FriendsApp.Server.Auth.Repository;
using FriendsApp.Server.Models;
using FriendsApp.Server.Models.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace FriendsApp.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IAuthRepository _repository;
        private readonly IConfiguration _configuration;

        public AuthController(IMapper mapper, IAuthRepository repository, IConfiguration configuration)
        {
            _mapper = mapper;
            _repository = repository;
            _configuration = configuration;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(UserRegistrationDTO userRegistrationDTO)
        {
            try
            {
                if (await _repository.UserExists(userRegistrationDTO.UserName))
                {
                    return BadRequest("User Name Already Exists!");
                }

                var userObj = _mapper.Map<User>(userRegistrationDTO);
                var userToCreate = await _repository.Register(userObj, userRegistrationDTO.Password);
                var userToReturn = _mapper.Map<UserDetailDto>(userToCreate);
                return CreatedAtRoute("GetUser", new {controller = "Users", id = userToCreate.Id}, userToReturn);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        [HttpPost("login")]
        public async Task<IActionResult> Login(UserLoginDTO userLoginDTO)
        {
            if (String.IsNullOrEmpty(userLoginDTO.UserName) || String.IsNullOrEmpty(userLoginDTO.Password))
            {
                return BadRequest("Invalid input request.");
            }

            var user = await _repository.Login(userLoginDTO.UserName, userLoginDTO.Password);
            if (user == null)
            {
                return Unauthorized();
            }
            // generate claim and claims types
            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName)
            };
            // generate key
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetSection("AppSettings:Token").Value));
            // generate credentials
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);
            // generate token descriptor
            var descriptor = new SecurityTokenDescriptor()
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = credentials
            };
            // token handaler
            var tokenHandaler = new JwtSecurityTokenHandler();
            var token = tokenHandaler.CreateToken(descriptor);

            var userToReturn = _mapper.Map<UserListDto>(user);
            return Ok(new
            {
                token = tokenHandaler.WriteToken(token),
                user = userToReturn
            });
        }
    }
}