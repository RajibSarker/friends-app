﻿using AutoMapper;
using FriendsApp.Models.Hubs;
using FriendsApp.Server.Helpers;
using FriendsApp.Server.Helpers.Paging;
using FriendsApp.Server.Interfaces;
using FriendsApp.Server.Models;
using FriendsApp.Server.Models.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FriendsApp.Server.Controllers
{
    [ServiceFilter(typeof(LogUserActivity))]
    [Authorize]
    [Route("api/users/{userId}/[controller]")]
    [ApiController]
    public class MessagesController : ControllerBase
    {
        private readonly IFriendsRepository _friendsRepository;
        private readonly IMapper _mapper;

        public MessagesController(IFriendsRepository friendsRepository, IMapper mapper)
        {
            _friendsRepository = friendsRepository;
            _mapper = mapper;
        }
        [HttpGet("{id}", Name = "GetMessage")]
        public async Task<IActionResult> GetMessage(int userId, int id)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var userFromRepo = await _friendsRepository.GetUser(userId);
            if (userFromRepo == null) return BadRequest("Could not find the user");

            var message = await _friendsRepository.GetMessage(id);
            if (message == null) return NotFound();

            return Ok(message);
        }

        [HttpGet]
        public async Task<IActionResult> GetMessages(int userId, [FromQuery] MessageParams messageParams)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            messageParams.UserId = userId;

            var messagesFromRepo = await _friendsRepository.GetMessagesForUser(messageParams);

            var dataToReturn = _mapper.Map<IEnumerable<MessageToReturnDto>>(messagesFromRepo);
            Response.AddPagination(messagesFromRepo.CurrentPage, messagesFromRepo.PageSize, messagesFromRepo.TotalCount, messagesFromRepo.TotalPages);

            return Ok(dataToReturn);

        }

        [HttpGet("thread/{recipientId}")]
        public async Task<IActionResult> GetMessageThread(int userId, int recipientId)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var messagesFromRepo= await _friendsRepository.GetMessageThread(userId, recipientId);
            var messageToReturn = _mapper.Map<IEnumerable<MessageToReturnDto>>(messagesFromRepo);

            return Ok(messageToReturn);
        }

        [HttpPost]
        public async Task<IActionResult> CreateMessage(int userId, MessageForCreation messageForCreation)
        {
            var sender = await _friendsRepository.GetUser(userId);
            if (sender == null)
                return NotFound($"No user found for user Id {userId}");

            if (sender.Id != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            messageForCreation.SenderId = userId;

            var recipient = await _friendsRepository.GetUser(messageForCreation.RecipientId);
            if (recipient == null) return BadRequest("Could not find the user");

            var message = _mapper.Map<Message>(messageForCreation);
            if (await _friendsRepository.Add<Message>(message))
            {
                // send the notification to the recipient end

                var isNotified = await _friendsRepository.SendNotificationToUser(recipient.Id, "OnMessageNotification", 
                    new HubResponseDto { SenderId = userId, RecipientId = recipient.Id, Message = "New Message!" });

                var messageToReturn = _mapper.Map<MessageToReturnDto>(message);
                return CreatedAtRoute("GetMessage", new { id = message.Id }, messageToReturn);
            }
            throw new Exception("Creation of message is failed!!!");
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMessage(int id, int userId)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var existingMessage = await _friendsRepository.GetMessage(id);
            if (existingMessage == null) throw new Exception("Message could not be found!");

            if (existingMessage.SenderId == userId)
                existingMessage.SenderDeleted = true;

            if (existingMessage.RecipientId == userId)
                existingMessage.RecipientDeleted = true;

            if (existingMessage.SenderDeleted && existingMessage.RecipientDeleted)
            {
                await _friendsRepository.Remove(existingMessage);
                return NoContent();
            }

            if (await _friendsRepository.SaveAll())
                return NoContent();

            throw new Exception("Error to delete the message!");
        }

        [HttpPost("{id}/read")]
        public async Task<IActionResult> MarkMessageAsRead(int userId, int id)
        {
            if(userId!=int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var existingMessage = await _friendsRepository.GetMessage(id);

            if(existingMessage.RecipientId != userId)
                return Unauthorized();

            existingMessage.IsRead = true;
            existingMessage.ReadDate = DateTime.Now;

            await _friendsRepository.SaveAll();
            return NoContent();
        }
    }
}
