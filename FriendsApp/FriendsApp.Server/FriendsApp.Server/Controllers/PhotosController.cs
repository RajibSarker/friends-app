﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using FriendsApp.Server.Helpers;
using FriendsApp.Server.Interfaces;
using FriendsApp.Server.Models;
using FriendsApp.Server.Models.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.Options;
using Microsoft.Extensions.Options;

namespace FriendsApp.Server.Controllers
{
    [Authorize]
    [Route("api/users/{userId}/photos")]
    [ApiController]
    public class PhotosController : ControllerBase
    {
        private readonly IFriendsRepository _repo;
        private readonly IMapper _mapper;
        private readonly IOptions<CloudinarySettings> _cloudinaryConfig;
        private Cloudinary _cloudinary;

        public PhotosController(IFriendsRepository _repo, IMapper mapper, IOptions<CloudinarySettings> cloudinaryConfig)
        {
            this._repo = _repo;
            _mapper = mapper;
            _cloudinaryConfig = cloudinaryConfig;

            Account acc = new Account(
                                        _cloudinaryConfig.Value.CloudName,
                                        _cloudinaryConfig.Value.ApiKey,
                                        _cloudinaryConfig.Value.ApiSecret
                                    );
            _cloudinary = new Cloudinary(acc);
        }
        // GET: api/Photos
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Photos/5
        [HttpGet("{id}", Name = "GetPhoto")]
        public async Task<IActionResult> GetPhoto(int id)
        {
            var photo = await _repo.GetPhoto(id);
            var photoToReturn = _mapper.Map<PhotoForReturnDto>(photo);
            return Ok(photoToReturn);
        }

        // POST: api/Photos
        [HttpPost]
        public async Task<IActionResult> Post(int userId, [FromForm] PhotoForCreationDto inputRequest)
        {
            try
            {
                if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                {
                    return BadRequest();
                }

                var userFromRepo = await _repo.GetUser(userId);

                var uploadedResult = new ImageUploadResult();
                var file = inputRequest.File;
                if (file != null && file.Length > 0)
                {
                    using (var stream = file.OpenReadStream())
                    {
                        var uploadParams = new ImageUploadParams()
                        {
                            File = new FileDescription(file.FileName, stream),
                            Transformation = new Transformation().Height(500).Width(500).Crop("fill").Gravity("face")
                        };
                        uploadedResult = _cloudinary.Upload(uploadParams);
                    }
                }

                inputRequest.Url = uploadedResult.Uri.ToString();
                inputRequest.PublicId = uploadedResult.PublicId;

                var photo = _mapper.Map<Photo>(inputRequest);
                if (!userFromRepo.Photos.Any(c => c.IsMain))
                    photo.IsMain = true;
                userFromRepo.Photos.Add(photo);
                if (await _repo.SaveAll())
                {
                    var photoToReturn = _mapper.Map<PhotoForReturnDto>(photo);
                    return CreatedAtRoute("GetPhoto", new { id = photo.Id }, photoToReturn);
                }

                return BadRequest("Could not added the photo");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("{id}/setMain")]
        public async Task<IActionResult> SetMainPhoto(int userId, int id)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            {
                return BadRequest();
            }

            var userFromRepo = await _repo.GetUser(userId);
            if (!userFromRepo.Photos.Any(c => c.Id == id))
            {
                return Unauthorized();
            }

            var photoFromRepo = await _repo.GetPhoto(id);
            if (photoFromRepo.IsMain)
            {
                return BadRequest("This photo is already the main photo.");
            }

            var currentMainPhoto = await _repo.GetMainPhotForUser(userId);
            currentMainPhoto.IsMain = false;

            photoFromRepo.IsMain = true;
            if (await _repo.SaveAll())
            {
                return NoContent();
            }

            return BadRequest("Could not set this photo as main photo. Please try again later.");
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePhoto(int userId, int id)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            {
                return BadRequest();
            }

            var userFromRepo = await _repo.GetUser(userId);
            if (!userFromRepo.Photos.Any(c => c.Id == id))
            {
                return Unauthorized();
            }

            var photoFromRepo = await _repo.GetPhoto(id);
            if (photoFromRepo.IsMain)
            {
                return BadRequest("You cannot delete your main photo.");
            }

            if (!string.IsNullOrEmpty(photoFromRepo.PublicId))
            {
                var result = _cloudinary.Destroy(new DeletionParams(photoFromRepo.PublicId));
                if (result.Result == "ok")
                {
                    //await _repo.Remove(photoFromRepo);
                    _repo.Delete(photoFromRepo);
                }
            }
            else
            {
                //await _repo.Remove(photoFromRepo);
                _repo.Delete(photoFromRepo);
            }

            if (await _repo.SaveAll())
            {
                return Ok();
            }

            return BadRequest("Photo could not be deleted.");
        }
        // PUT: api/Photos/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }
    }
}
