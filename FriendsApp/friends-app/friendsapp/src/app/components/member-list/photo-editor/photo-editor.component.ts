import { AlertifyService } from '../../../_services/alertify.service';
import { UserService } from './../../../_services/user.service';
import { AuthService } from './../../../_services/auth.service';
import { environment } from './../../../../environments/environment';
import { Photo } from './../../../_models/photo';
import { Component, Input, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';

@Component({
  selector: 'app-photo-editor',
  templateUrl: './photo-editor.component.html',
  styleUrls: ['./photo-editor.component.css']
})
export class PhotoEditorComponent implements OnInit {
  uploader: FileUploader;
  hasBaseDropZoneOver: boolean;
  response: string;
  baseUrl = environment.apiUrl;
  currentMainPhoto: Photo;

  @Input() photos: Photo[];
  @Output() getMainPhotoUrlForUser = new EventEmitter<string>();
  constructor(
    private authService: AuthService,
    private userService: UserService,
    private alertify: AlertifyService
  ) { }

  ngOnInit() {
    this.initializeUploader();
  }
  fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }
  initializeUploader() {
    this.uploader = new FileUploader({
      url: this.baseUrl + 'users/' + this.authService.decodeToken.nameid + '/photos',
      allowedFileType: ['image'],
      autoUpload: false,
      removeAfterUpload: true,
      maxFileSize: 10 * 1024 * 1024,
      authToken: 'Bearer ' + localStorage.getItem('token'),
      isHTML5: true
    });
    // to solve cors origin error solve
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false };
    // after uploaded will call this
    this.uploader.onSuccessItem = (item, response, status, headers) => {
      const res: Photo = JSON.parse(response);
      const photo = {
        id: res.id,
        url: res.url,
        dateAdded: res.dateAdded,
        description: res.description,
        isMain: res.isMain
      };
      this.photos.push(photo);
      if(photo.isMain){
        this.authService.changeMemberPhoto(photo.url);
        this.authService.currentUser.photoUrl = photo.url;
        localStorage.setItem('user', JSON.stringify(this.authService.currentUser));
      }
    };
  }
  // ser user main photo
  setUserMainPhoto(photo: Photo) {
    if (photo) {
      this.userService.setUserMainPhoto(this.authService.decodeToken.nameid, photo.id).subscribe(() => {
        console.log('Photo set as main photo.');
        this.currentMainPhoto = this.photos.filter(c => c.isMain === true)[0];
        this.currentMainPhoto.isMain = false;
        photo.isMain = true;
        // this.getMainPhotoUrlForUser.emit(photo.url);
        this.authService.changeMemberPhoto(photo.url);
        this.authService.currentUser.photoUrl = photo.url;
        localStorage.setItem('user', JSON.stringify(this.authService.currentUser));
      }, error => {
        this.alertify.error(error);
      });
    } else {
      this.alertify.warning('Please select an image to set as main photo.');
    }
  }
  // delete user photo
  deleteMemberPhoto(id: number) {
    this.alertify.confirm('Are you sure you want to delete this photo?', () => {
      this.userService.deleteMemberPhoto(this.authService.decodeToken.nameid, id).subscribe(() => {
        this.photos.splice(this.photos.findIndex(c => c.id === id), 1);
        this.alertify.success('Photo delete successful.');
      }, error => {
        this.alertify.error(error);
      });
    })
  }
}
