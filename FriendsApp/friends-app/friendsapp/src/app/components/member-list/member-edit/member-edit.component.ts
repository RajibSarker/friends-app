import { UserService } from './../../../_services/user.service';
import { AuthService } from './../../../_services/auth.service';
import { AlertifyService } from '../../../_services/alertify.service';
import { User } from './../../../_models/user';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild, OnDestroy, HostListener } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-member-edit',
  templateUrl: './member-edit.component.html',
  styleUrls: ['./member-edit.component.css']
})
export class MemberEditComponent implements OnInit {

  photoUrl: string;
  @ViewChild('editForm', { static: false }) editForm: NgForm;
  user: User;
  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    if (this.editForm.dirty) {
      $event.returnValue = true;
    }
  }

  constructor(private route: ActivatedRoute,
    private alertify: AlertifyService,
    private authService: AuthService,
    private userService: UserService) { }

  ngOnInit() {
    this.route.data.subscribe(res => {
      this.user = res['user'];
    });
    this.authService.currentPhotoUrl.subscribe(photoUrl=> this.photoUrl = photoUrl);
  }

  onSubmit() {
    this.userService.updateUser(this.authService.decodeToken.nameid, this.user).subscribe(next => {
      this.alertify.success('Profile updated successfully.');
      this.editForm.reset(this.user);
    }, error => {
      this.alertify.error(error);
    });
  }
  updateUserMainPhoto(photoUrl) {
    if (this.user) {
      console.log(this.user);
      this.user.photoUrl = photoUrl;
    }
  }
}
