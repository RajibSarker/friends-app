import { PaginatedResult } from './../../_models/pagination/pagination';
import { UserService } from './../../_services/user.service';
import { AlertifyService } from '../../_services/alertify.service';
import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_models/user';
import { ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { Pagination } from 'src/app/_models/pagination/pagination';

@Component({
  selector: 'app-member-list',
  templateUrl: './member-list.component.html',
  styleUrls: ['./member-list.component.css']
})
export class MemberListComponent implements OnInit {
  pagination: Pagination;
  users: User[];
  user: User = JSON.parse(localStorage.getItem('user'));
  genderList = [{ value: 'male', display: 'Male' }, { value: 'female', display: 'Female' }];
  userParams: any = {};
  constructor(
    private alertify: AlertifyService,
    private userService: UserService,
    private router: ActivatedRoute
  ) { }

  ngOnInit() {
    // this.getUsers();
    this.router.data.subscribe(res => {
      this.users = res['users'].result;
      this.pagination = res['users'].pagination;
      console.log(this.pagination);
    });

    this.userParams.gender = this.user.gender === 'male' ? 'female' : 'male';
    this.userParams.minAge = 18;
    this.userParams.maxAge = 99;
    this.userParams.orderBy = 'lastActive';
  }
  resetFilters() {
    this.userParams.gender = this.user.gender === 'male' ? 'female' : 'male';
    this.userParams.minAge = 18;
    this.userParams.maxAge = 99;
    this.userParams.orderBy = 'lastActive';
    this.getUsers();
  }
  pageChanged(event) {
    this.pagination.currentPage = event.page;
    this.getUsers();
  }
  getUsers() {
    this.userService.getUsers(this.pagination.itemsPerPage, this.pagination.currentPage, this.userParams).subscribe((res: PaginatedResult<User[]>) => {
      this.users = res.result;
      this.pagination = res.pagination;
      console.log(this.pagination);
    }, error => {
      this.alertify.error(error);
    });
  }
}
