import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Message } from 'src/app/_models/message';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { AuthService } from 'src/app/_services/auth.service';
import { UiSyncService } from 'src/app/_services/uiSync.service';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'app-member-message',
  templateUrl: './member-message.component.html',
  styleUrls: ['./member-message.component.css']
})
export class MemberMessageComponent implements OnInit, OnDestroy {

  @Input() recipentId = 0;
  messages: Message[];
  newMessage: any = {};

  onMessageNotification: any;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private alertify: AlertifyService,
    private uiSyncService: UiSyncService
  ) {
    this.onMessageNotification = this.uiSyncService.onMessageNotification.subscribe((res: any) => {
      if (res && res.senderId === this.recipentId && res.recipientId === this.authService.currentUser.id) {
        this.getMessageThread();
      }
    })
  }


  ngOnDestroy(): void {
    this.onMessageNotification.unsubscribe();
  }

  ngOnInit() {
    this.getMessageThread();
  }

  getMessageThread() {
    const currentUserId = +this.authService.decodeToken.nameid;
    this.userService.getMessageThread(this.authService.decodeToken.nameid, this.recipentId)
      .pipe(
        tap((messages: any) => {
          for (let i = 0; i < messages.length; i++) {
            if (messages[i].recipientId === currentUserId && messages[i].isRead === false) {
              this.userService.markMessageAsRead(currentUserId, messages[i].id);
            }
          }
        })
      )
      .subscribe((messages: Message[]) => {
        this.messages = messages;
      }, err => {
        this.alertify.error(err);
      });
  }

  sendMessage() {

    this.newMessage.recipientId = this.recipentId;
    this.userService.sendMessage(this.authService.decodeToken.nameid, this.newMessage).subscribe((res: Message) => {
      this.messages.unshift(res);
      this.newMessage.content = '';
    }, error => {
      this.alertify.error(error);
    })
  }
}
