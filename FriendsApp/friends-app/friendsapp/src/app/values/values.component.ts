import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-values",
  templateUrl: "./values.component.html",
  styleUrls: ["./values.component.css"],
})
export class ValuesComponent implements OnInit {
  baseAPI = "http://localhost:5000/api/";
  values: any[] = [];
  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.getValues();
  }

  getValues() {
    this.http.get(this.baseAPI + "values").subscribe(
      (response: any[]) => {
        this.values = response;
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
