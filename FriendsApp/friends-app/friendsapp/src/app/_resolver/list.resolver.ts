import { Observable, of } from 'rxjs';
import { AlertifyService } from '../_services/alertify.service';
import { UserService } from './../_services/user.service';
import { User } from './../_models/user';
import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ListResolver implements Resolve<User[]>{
    pageSize = 5;
    pageNumber = 1
    likesParam = 'Likers';

    constructor(private router: Router, private userService: UserService, private alertify: AlertifyService) {
    }
    resolve(data: ActivatedRouteSnapshot): Observable<User[]> {
        return this.userService.getUsers(this.pageSize, this.pageNumber, null, this.likesParam).pipe(
            catchError(error => {
                this.alertify.error('Problem to retrive data.');
                this.router.navigate(['/home']);
                return of(null);
            })
        );
    }
}