import { Observable, of } from 'rxjs';
import { UserService } from './../_services/user.service';
import { AlertifyService } from '../_services/alertify.service';
import { User } from './../_models/user';
import { Injectable } from "@angular/core";
import { Resolve, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { catchError } from 'rxjs/operators';

@Injectable()
export class MemberDetailResolver implements Resolve<User>{
    constructor(private router: Router,
        private alertify: AlertifyService,
        private userService: UserService
    ) { }

    resolve(data: ActivatedRouteSnapshot): Observable<User> {
        return this.userService.getUser(data.params['id']).pipe(
            catchError(error => {
                this.alertify.error('Problem retriving data.');
                this.router.navigate(['/members']);
                return of(null);
            })
        );
    }
}

