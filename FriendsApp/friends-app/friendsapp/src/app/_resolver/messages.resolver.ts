import { Observable, of } from 'rxjs';
import { AlertifyService } from '../_services/alertify.service';
import { UserService } from '../_services/user.service';
import { User } from '../_models/user';
import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { AuthService } from '../_services/auth.service';

@Injectable()
export class MessagesResolver implements Resolve<Message[]>{
    pageSize = 5;
    pageNumber = 1
    messageContainer = 'Unread';
    constructor(private router: Router, private userService: UserService,
        private authService: AuthService,
        private alertify: AlertifyService) {
    }
    resolve(data: ActivatedRouteSnapshot): Observable<Message[]> {
        return this.userService.getMessages(this.authService.currentUser.id, this.pageNumber, this.pageSize, this.messageContainer).pipe(
            catchError(error => {
                this.alertify.error('Problem to retrive data.');
                this.router.navigate(['/home']);
                return of(null);
            })
        );
    }
}