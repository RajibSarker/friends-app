import { User } from 'src/app/_models/user';
import { AuthService } from './_services/auth.service';
import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';
import { SignalRService } from './_services/signalR.service';
import { AlertifyService } from './_services/alertify.service';
import { UiSyncService } from './_services/uiSync.service';
import { HubResponseDto } from './_models/_hubs/hub-response-dto';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  jwtHelper = new JwtHelperService();
  title = 'friendsapp';
  messageUrl = environment.signalRUrl;
  isShowMessagePopup = false;
  popupMessage = '';


  constructor(private authService: AuthService,
    private signalRService: SignalRService,
    private alertify: AlertifyService,
    private uiSyncService: UiSyncService
  ) {
    // re-generate the signlR when loggedout
    this.uiSyncService.onLoggedOut.subscribe(res => {
      debugger
      if (res) {
        this.authService.updateUserConnection();
        this.startSignaling(this.messageUrl);
      }
    })

    // start the signalR process
    this.startSignaling(this.messageUrl);
  }


  ngOnInit() {
    const token = localStorage.getItem('token');
    const user: User = JSON.parse(localStorage.getItem('user'));
    if (token) {
      this.authService.decodeToken = this.jwtHelper.decodeToken(token);
    }
    if (user) {
      this.authService.currentUser = user;
      this.authService.changeMemberPhoto(this.authService.currentUser.photoUrl);
    }
  }


  //#region manage user connections

  // star the signalR process
  startSignaling(path: string) {
    this.signalRService.connect(path, true).then(() => {
      if (this.signalRService.isConnected()) {
        this.authService.userSignalRConnectionId = this.signalRService.signalRConnectionId;
        this.authService._userSignalRConnectionId.next(this.signalRService.signalRConnectionId);
        // keep the user connection
        this.authService.addUserConnection();

        this.defineSignaling();
      }
    });
  }

  // define signalR events
  defineSignaling(): void {
    // re-connected signalR
    this.signalRService.onReconnected((data: string) => {
      this.signalRService.onReconnectedId().then((connectionid: any) => {
        this.authService.userSignalRConnectionId = connectionid;
        this.authService._userSignalRConnectionId.next(connectionid);
        // keep the user connection
        this.authService.addUserConnection();

        console.log('my reconnected id: ' + connectionid);
      });
    })

    // signalR events
    this.signalRService.define('OnUserResultProcessed', (data: HubResponseDto) => {
      this.alertify.message(data.Message);
    });

    this.signalRService.define('OnEmployeeResultProcessed', (data: HubResponseDto) => {
      debugger
      console.log('On OnEmployeeResultProcessed', data);
      console.log('Current user', this.authService.currentUser);
      // if (this.authService.currentUser && this.authService.currentUser.clientId > 0 && data.ClientId === this.authService.currentUser.clientId && data.CatalyzrPersonId === this.authService.currentUser.catalyzrPersonId && this.authService.selectedRoleId === RoleEnum.Employee) {
      //   // this.uiSyncService.isEmployeeResultProcessed.next(true);
      //   // this.alertify.message(data.Message);
      //   this.isShowMessagePopup = true;
      //   this.popupMessage = data.Message;
      // }
    });

    // message sent/received notification
    this.signalRService.define('OnMessageNotification', (data: HubResponseDto) => {
      debugger
      this.uiSyncService.onMessageNotification.next({ senderId: data.SenderId, recipientId: data.RecipientId, message: data.Message });
      this.alertify.message(data.Message);
    })

  }
  //#endregion
}
