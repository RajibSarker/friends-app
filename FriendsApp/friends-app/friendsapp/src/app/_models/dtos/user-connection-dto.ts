export class UserConnectionDto{
    id: number;
    userId: number;
    connectionId: string;
    isActive: boolean;
    deviceInfo: string;
    createdById: number;
}