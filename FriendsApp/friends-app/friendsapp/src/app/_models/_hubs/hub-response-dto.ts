export class HubResponseDto{
    public ErrorHeaderId: number;
    public SenderId: number;
    public RecipientId: number;
    public ConnectionId: string;
    public Message: string;
    public TotalRowCount: number;
    public RowProcessCount: number;
    public IsCompletedRowProcess: boolean;
    public FileUploadProcessType: number;

    // file processing
    public ProcessId: number;
    public FileUploadedStatus: number;
    public FileName: string;
    public FileType: string;
    public FileUploadType: number;
    public LogId: number;
    public OnSuccess: boolean;
    public OnFailed: boolean;
    public IsProcessing: boolean;
}