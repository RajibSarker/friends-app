import { AlertifyService } from '../_services/alertify.service';
import { AuthService } from './../_services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UiSyncService } from '../_services/uiSync.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  photoUrl: string;
  model: any = {};
  constructor(
    public authService: AuthService,
    private alertify: AlertifyService,
    private router: Router,
    private uiSyncService: UiSyncService
  ) { }

  ngOnInit() {
    this.authService.currentPhotoUrl.subscribe(photoUrl => this.photoUrl = photoUrl);
  }

  // login form
  login() {
    this.authService.login(this.model)
      .subscribe(response => {
        this.alertify.success('Logged in successful.');
        this.router.navigate(['members']);
      }, error => {
        this.alertify.error(error)
      });
  }

  // checked logged in or not
  loggedIn() {
    return this.authService.loggedIn();
  }

  // logout
  logout() {
    this.uiSyncService.onLoggedOut.next(true);
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this.authService.decodeToken = null;
    this.authService.currentUser = null;
    this.alertify.message('Logout successful.');
    this.router.navigate(['home']);
  }
}
