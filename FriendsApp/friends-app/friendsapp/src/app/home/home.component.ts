import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  registerMode = false;
  constructor() { }

  ngOnInit() {
  }

  // register
  register() {
    this.registerMode = true;
  }

  // on cancel register mode
  onCancelRegisterMode(registerMode: boolean) {
    this.registerMode = registerMode;
  }
}
