import { Injectable } from '@angular/core';
import * as signalR from '@microsoft/signalr';
import { HubConnection, HubConnectionBuilder, HubConnectionState } from '@microsoft/signalr';

@Injectable({
  providedIn: 'root'
})
export class SignalRService {

  signalRConnectionId: string;
  private hubConnection: HubConnection | undefined;

  constructor() { }

  async connect(path: string, withToken: boolean): Promise<void> {
    this.hubConnection = new HubConnectionBuilder().withUrl(path, {
      skipNegotiation: true,
      transport: signalR.HttpTransportType.WebSockets
    })
      .configureLogging(signalR.LogLevel.Trace)
      .withAutomaticReconnect()
      .build();

    // this.hubConnection.keepAliveIntervalInMilliseconds = 900000;    // 15 minutes
    // this.hubConnection.serverTimeoutInMilliseconds = 1800000;   // 30 minutes

    this.hubConnection.keepAliveIntervalInMilliseconds = 30000;    // 30 sec
    this.hubConnection.serverTimeoutInMilliseconds = 60000;   // 1 minutes


    return this.hubConnection
      .start()
      .then(() => console.log("Connection started!"))
      .catch((err) => console.error('SignalR: Failed to start with error: ' + err.toString()))
      .then(() => this.hubConnection.invoke("getConnectionId"))
      .then((connectionId) => {
        (this.signalRConnectionId = connectionId),
          console.log("ConnectionId: ", this.signalRConnectionId);
      });
  }

  async define(methodName: string, newMethod: (...args: any[]) => void): Promise<void> {
    if (this.hubConnection) {
      this.hubConnection.on(methodName, newMethod);
    }
  }

  async invoke(methodName: string, ...args: any[]): Promise<any> {
    if (this.isConnected()) {
      return this.hubConnection.invoke(methodName, ...args);
    }
  }

  disconnect(): void {
    if (this.isConnected()) {
      this.hubConnection.stop();
    }
  }

  isConnected(): boolean {
    return this.hubConnection && this.hubConnection.state === HubConnectionState.Connected;
  }

  //#region signalR reconnection functionality
  // re connect signalR
  async onReconnected(newMethod: (...args: any[]) => void): Promise<void> {
    if (this.hubConnection) {
      return this.hubConnection.onreconnected(newMethod);
    }
  }

  async onReconnectedId(): Promise<void> {
    if (this.hubConnection) {
      return this.hubConnection.invoke('getConnectionId');
    }
  }
  //#endregion

}
