import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Message } from '../_models/message';
import { PaginatedResult } from '../_models/pagination/pagination';
import { User } from '../_models/user';


const httpOptions = {
  headers: new HttpHeaders({
    'Authorization': 'Bearer rajib' + localStorage.getItem('token')
  })
};
@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  getUsers(pageSize?, pageNumber?, userParams?, likesParam?): Observable<PaginatedResult<User[]>> {
    const paginatedResult: PaginatedResult<User[]> = new PaginatedResult<User[]>();
    let params: HttpParams = new HttpParams();
    if (pageSize && pageNumber) {
      params = params.append('pageSize', pageSize);
      params = params.append('pageNumber', pageNumber);
    }
    if (userParams) {
      params = params.append('minAge', userParams.minAge);
      params = params.append('maxAge', userParams.maxAge);
      params = params.append('gender', userParams.gender);
      params = params.append('orderBy', userParams.orderBy);
    }

    if (likesParam && likesParam === 'Likers') {
      params = params.append('Likers', 'true');
    }
    if (likesParam && likesParam === 'Likees') {
      params = params.append('Likees', 'true');
    }

    return this.http.get<User[]>(this.baseUrl + 'users', { observe: 'response', params: params })
      .pipe(
        map(response => {
          paginatedResult.result = response.body;
          if (response.headers.get('Pagination')) {
            paginatedResult.pagination = JSON.parse(response.headers.get('Pagination'));
          }
          return paginatedResult;
        })
      );
  }
  getUser(id: number): Observable<User> {
    return this.http.get<User>(this.baseUrl + 'users/' + id);
  }
  updateUser(id: number, user: User) {
    return this.http.put(this.baseUrl + 'users/' + id, user);
  }
  setUserMainPhoto(userId: number, photoId: number) {
    return this.http.post(this.baseUrl + 'users/' + userId + '/photos/' + photoId + '/setMain', {});
  }
  deleteMemberPhoto(userId: number, photoId: number) {
    return this.http.delete(this.baseUrl + 'users/' + userId + '/photos/' + photoId);
  }
  sendLike(userId: number, recipentId: number) {
    return this.http.post(this.baseUrl + 'users/' + userId + '/like/' + recipentId, {});
  }

  getMessages(id: number, currentPage?, itemPerPage?, messageContainer?) {
    var paginatedResult: PaginatedResult<Message[]> = new PaginatedResult<Message[]>();

    let params = new HttpParams();
    if (messageContainer) {
      params = params.append('MessageContainer', messageContainer);
    }
    if (currentPage && itemPerPage) {
      params = params.append('PageSize', itemPerPage);
      params = params.append('pageNumber', currentPage);
    }

    return this.http.get<Message[]>(this.baseUrl + 'users/' + id + '/messages', { observe: 'response', params })
      .pipe(
        map((response) => {
          paginatedResult.result = response.body;
          if (response.headers.get('Pagination')) {
            paginatedResult.pagination = JSON.parse(response.headers.get('Pagination'));
          }
          return paginatedResult;
        })
      )
  }

  getMessageThread(id: number, recipentId: number) {
    return this.http.get(this.baseUrl + 'users/' + id + '/messages/thread/' + recipentId);
  }

  sendMessage(id: number, message: any) {
    return this.http.post(this.baseUrl + 'users/' + id + '/messages', message);
  }

  deleteMessage(id: number, userId: number) {
    return this.http.delete(this.baseUrl + 'users/' + userId + '/messages/' + id, {});
  }

  markMessageAsRead(userId: number, id: number) {
    this.http.post(this.baseUrl + 'users/' + userId + '/messages/' + id + '/read', {}).subscribe();
  }
}
