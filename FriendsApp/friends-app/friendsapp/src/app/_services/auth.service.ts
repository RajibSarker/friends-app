import { Photo } from './../_models/photo';
import { User } from 'src/app/_models/user';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject, Subject } from 'rxjs';
import { UserConnectionDto } from '../_models/dtos/user-connection-dto';
import { UserConnectionsService } from './user-connections.service';
import { AlertifyService } from './alertify.service';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl = 'http://localhost:5000/api/';
  jwtHelper = new JwtHelperService();
  decodeToken: any;
  currentUser: User;

  photoUrl = new BehaviorSubject<string>('assets/images/user.png');
  currentPhotoUrl = this.photoUrl.asObservable();

  userSignalRConnectionId = '';
  public _userSignalRConnectionId = new Subject<string>();
  userSignalRConnectionId$ = this._userSignalRConnectionId.asObservable();

  constructor(private http: HttpClient,
    private alertify: AlertifyService,
    private userConnectionService: UserConnectionsService) { }

  changeMemberPhoto(photoUrl: string){
    this.photoUrl.next(photoUrl);
  }
  // login method
  login(model: any) {
    return this.http.post(this.baseUrl + 'auth/login', model)
      .pipe(
        map((response: any) => {
          const user = response;
          if (user) {
            localStorage.setItem('token', user.token);
            localStorage.setItem('user', JSON.stringify(user.user));
            this.currentUser = user.user;
            this.decodeToken = this.jwtHelper.decodeToken(user.token);
            this.changeMemberPhoto(this.currentUser.photoUrl);
          }
        })
      );
  }

  // user register method
  register(user: User) {
    return this.http.post(this.baseUrl + 'auth/register', user);
  }

  // check the valid token
  loggedIn() {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }

  //#region manage user connections
  addUserConnection() {
    if (this.currentUser && this.userSignalRConnectionId) {
      var inputRequest = new UserConnectionDto();
      inputRequest.userId = this.currentUser.id;
      inputRequest.connectionId = this.userSignalRConnectionId;
      inputRequest.createdById = this.currentUser.id;
      inputRequest.deviceInfo = 'Browser: ';
      // inputRequest.deviceInfo = 'Browser: ' + this.deviceDetector.browser + ', V:- ' + this.deviceDetector.browser_version + ', OS: ' + this.deviceDetector.os + ', V:- ' + this.deviceDetector.os_version;
      inputRequest.isActive = true;

      this.userConnectionService.addUserConnection(inputRequest).subscribe(res => {
        console.log('User Connection Saved: ', res);
      }, error => {
        this.alertify.error(error);
      });
    }
  }

  updateUserConnection() {
    if (this.currentUser && this.userSignalRConnectionId) {
      var inputRequest = new UserConnectionDto();
      // inputRequest.userId = this.currentUser.id;
      inputRequest.connectionId = this.userSignalRConnectionId;
      // inputRequest.createdById = this.currentUser.id;
      inputRequest.isActive = false;

      this.userConnectionService.updateUserConnection(inputRequest).subscribe(res => {
        console.log('User Connection Updated: ', res);
      }, error => {
        this.alertify.error(error);
      });
    }
  }

  getUserActiveConnections() {
    if (this.currentUser) {
      this.userConnectionService.getUserActiveConnections(this.currentUser.id).subscribe(res => {
        console.log('User Active Connections: ', res);
      }, error => {
        this.alertify.error(error);
      });
    }
  }
  //#endregion
}
