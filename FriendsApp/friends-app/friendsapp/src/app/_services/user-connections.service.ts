import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { UserConnectionDto } from '../_models/dtos/user-connection-dto';

const httpOptions = {
  headers: new HttpHeaders(
    {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }
  )
};

@Injectable({
  providedIn: 'root'
})
export class UserConnectionsService {

baseAPI = environment.apiUrl;
  constructor(private http: HttpClient) { }

  getUserActiveConnections(userId: number) {
    return this.http.get(this.baseAPI + 'ManageUserConnections/user/' + userId + '/activeConnections', { headers: httpOptions.headers });
  }
  addUserConnection(inputRequest: UserConnectionDto) {
    return this.http.post(this.baseAPI + 'ManageUserConnections/addUserConnection', inputRequest, { headers: httpOptions.headers });
  }
  updateUserConnection(inputRequest: UserConnectionDto) {
    return this.http.put(this.baseAPI + 'ManageUserConnections/updateUserConnection', inputRequest, { headers: httpOptions.headers });
  }

}
