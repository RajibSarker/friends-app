import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UiSyncService {

  constructor() { }

  onLoggedOut: Subject<boolean> = new Subject<boolean>();
  onMessageNotification: Subject<{ senderId: number, recipientId: number, message: string }> = new Subject<{ senderId: number, recipientId: number, message: string }>();
}
